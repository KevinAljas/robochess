#include "comm.hpp"


#define PORT_NAME "/dev/ttyS0"
#define PACKET_LEN 7

// UART variables
static int serial_port;

void UART_Init(void) {
    serial_port = open(PORT_NAME, O_RDWR);
    if (serial_port < 0) {
        std::cerr << "Can't open: " << PORT_NAME << std::endl;
        exit(1);
    }
}

void UART_SendMoveToNucleo(std::string move) {
   	printf("Starting print!\n\r");
	// Consturct packet with byte-sum
    uint8_t byte_sum = 'S' + 'E';
    uint8_t packet[PACKET_LEN] = {0};

    packet[0] = 'S';
    packet[PACKET_LEN - 2] = 'E';

    for (uint8_t i = 1; i < 5; i++) {
	packet[i] = move[i - 1];
	byte_sum += move[i - 1];
    }

    packet[PACKET_LEN - 1] = byte_sum;

    // Send packet until proper checksum is reached
    uint8_t response = 0;
    do {
        write(serial_port, packet, PACKET_LEN);
        read(serial_port, &response, 1);
	std::cout << "We received: " << response << std::endl;
    } while(response != byte_sum);    

    printf("Success!\n\r");
}

