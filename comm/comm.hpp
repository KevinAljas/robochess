#ifndef COMM_H_
#define COMM_H_

#include "termios.h"

#include <string.h>
#include <iostream>
#include <fcntl.h>
#include <cstdint>
#include <unistd.h>
#include <errno.h>

void UART_Init();
void UART_SendMoveToNucleo(std::string move);

#endif