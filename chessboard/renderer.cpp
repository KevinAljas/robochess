#include "renderer.hpp"

#include <iostream>
#include "grid.hpp"

using std::cout, std::endl;

void PrintTable(grid_t<Base*>& table) {
    system("clear");

    // Add top border
    for (int width = 0; width < 8 * 11 + 3; width++) {
        cout << "-";
    }
    cout << endl;
    
    for (int y = 0; y < 8; y++) {
        // Each piece has 5 layers of height
        for (int layer = 0; layer < 5; layer++) {
            cout << ":";
            cout << Chess_Textures::NUMBERS[7 - y][layer];
            cout << ":";

            for (int x = 0; x < 8; x++) {
                if (table[point_t {x, y}] == 0) {
                    cout << Chess_Textures::BLANK[layer];
                    cout << ":";
                    continue;
                }

                int piece_type = (int) table[point_t {x, y}]->type();
                int piece_color = (int) table[point_t {x, y}]->color;

                cout << Chess_Textures::TEXTURES[piece_type + (piece_color * 6)][layer];
                cout << ":";
            }

            cout << endl;
        }

        // Print square grid lines
        for (int width = 0; width < 8 * 11 + 3; width++) {
            cout << "-";
        }
        cout << endl;
    } 

    // Print letters to the bottom
    for (int layer = 0; layer < 5; layer++) {
        cout << ":" << Chess_Textures::BLANK[layer];

        for (int x = 0; x < 8; x++) {
            cout << ":";
            cout << Chess_Textures::ALPHABET[x][layer];
        }
        cout << ":" << endl;
    }

    // Print final grid line
    for (int width = 0; width < 8 * 11 + 3; width++) {
        cout << "-";
    }
    cout << endl;
}