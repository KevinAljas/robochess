#ifndef CHESSBOARD_H_
#define CHESSBOARD_H_

#include "coords.hpp"
#include "grid.hpp"
#include "logger.hpp"
#include <cstdint>

// Forward declarations to solve circular dependancy
class Base;
enum class color_n; 

class Chessboard {
public:
    grid_t<Base*> table;
    bool is_original = true; // Used to keep track if class instance is original or copy
    int moves_since_capture = 0; // If reaches 50, call it a draw

    // Keep track of previous moves
    Base* previous_piece;
    std::string previous_move;
    std::string previous_eight_moves[8];
    int move_count = 0;

    // King locations - simplifies checking "checks"
    point_t kings[2];
    
    // Stores the promotion a pawn will get, assume queen by default
    char next_promotion = 'q';

    Chessboard(void);

    template <class PIECE>
    void AddPiece(point_t coord, color_n color);
    void PromotePiece(point_t coord);

    void SetupPieces(void);
    void CleanPieces(void);

    bool ValidateMove(std::string move);
    void MakeMove(std::string move);
    bool CheckIfSquareSafe(point_t square, color_n threat_color);
    
    // Functions for external use, for API and RC classes
    color_n LastMoveColor(void);
    bool OnlyKingsRemain(void);
    bool IsThreefoldRepetition(void);

    Base* operator[] (point_t b) {
        return table[b];
    }
};

#endif
