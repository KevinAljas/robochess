#ifndef COORDS_H_
#define COORDS_H_

#include <iostream>
#include <cstdint>

struct Point {
    int x = 0;
    int y = 0;

    // Addition
    Point operator+ (const Point b) const {
       return Point {this->x + b.x, this->y + b.y}; 
    }

    void operator+= (const Point b) {
        this->x += b.x;
        this->y += b.y;
    }

    // Subtraction
    Point operator- (const Point b) const {
        return Point {this->x - b.x, this->y - b.y}; 
    }

    void operator-= (const Point b) {
        this->x -= b.x;
        this->y -= b.y;
    }

    // Scalar multiplication
    friend Point operator* (const Point a, const int b) {
        return Point {a.x * b, a.y * b};
    }

    friend Point operator* (const int a, const Point b) {
        return Point {a * b.x, a * b.y};
    }

    // Scalar division
    Point operator/ (const int b) {
        this->x /= b;
        this->y /= b;
        return *this;
    }

    // Equality
    bool operator== (const Point b) {
        return ((this->x == b.x) && (this->y == b.y));
    }

    // Divison - divide matching elements
    Point operator/ (const Point b) {
        this->x /= b.x;
        this->y /= b.y;
        return *this;
    }

    void operator/= (const Point b) {
        this->x /= b.x;
        this->y /= b.y;
    }

    // Greater than
    bool operator> (const Point b) const {
        return ((this->x > b.x) && (this->y > b.y));
    }

    // Greater Equal than
    bool operator>= (const Point b) const {
        return ((this->x >= b.x) && (this->y >= b.y));
    }

    // Lesser than
    bool operator< (const Point b) const {
        return ((this->x < b.x) && (this->y < b.y));
    }

    // Lesser Equal than
    bool operator<= (const Point b) const {
        return ((this->x <= b.x) && (this->y <= b.y));
    }

    // Inequality
    bool operator!= (const Point b) const {
        return ((this->x != b.x) || (this->y != b.y));
    }

    // Standard output
    friend std::ostream& operator<<(std::ostream& os, const Point a) {
        os << "(" << a.x << ", " << a.y << ")";
        return os;
    }
};

using point_t = Point;

#endif
