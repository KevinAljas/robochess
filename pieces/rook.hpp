#ifndef ROOK_H_
#define ROOK_H_

#include "base.hpp"

class Rook : public Base {
public:
    Rook(color_n new_color, point_t new_coords) {
        name = "Rook";

        color = new_color;

        has_moved = false;
        coords = new_coords;
    }   


    piece_n type() const {
        return piece_n::ROOK;
    }   

    ~Rook() {
        return;
    }

    bool ValidateMove(const point_t start, const point_t end, Chessboard& board) {
		point_t move_vector = end - start;

        // Horizontal / Vertical moves means change on one axis must be 0.
        if (move_vector.x != 0 && move_vector.y != 0) {
            return false;
        }

        // If target square contains friendly piece, don't eat it.
        if (board.table[end] != 0 && board.table[end]->color == color) {
            return false;
        }

        // Normalize move_vector so that values are either -1, 0 or 1. This gives us a heading which way the rook is trying to move.
        move_vector.x = move_vector.x ? move_vector.x / std::abs(move_vector.x) : 0;
        move_vector.y = move_vector.y ? move_vector.y / std::abs(move_vector.y) : 0;

        // Check if path is clear, do so by checking every square between start and end, by incrementing with the normalized move_vector.
        point_t check_start = start + move_vector;
        point_t check_end = end;
        while(check_start != check_end) {
            if (board.table[check_start] != 0) {
                return false; // Piece is in the way of target square
            }

            check_start += move_vector;
        }

        return true;
    }

    void MakeMove(const point_t start, const point_t end, Chessboard& board) {
        board.table[end] = board[start];
        board.table[start] = 0;
    }
};

#endif
