#ifndef GAME_LOGIC_H_
#define GAME_LOGIC_H_

#include "main.hpp"

#include "chessboard.hpp"
#include "api.hpp"
#include "renderer.hpp"
#include "comm.hpp"

void StartChess(int pipe_to_sf[2], int pipe_to_rc[2]);

#endif
