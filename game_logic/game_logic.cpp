#include "game_logic.hpp"

void StartChess(int pipe_to_sf[2], int pipe_to_rc[2]) {
    UART_Init();
    while(1) {
	UART_SendMoveToNucleo("A1A2");
    } 
    return;

    Robochess api = Robochess(pipe_to_rc, pipe_to_sf);
    api.IgnoreHeader();

    Chessboard board = Chessboard();
    PrintTable(board.table);

    // Main gameloop - Stockfish only means that Stockfish plays itself - used for testing
    #define STOCKFISH_ONLY 1	
    #if STOCKFISH_ONLY == 0
    bool player_turn = true;
    #else
    const bool player_turn = false;
    #endif

    api.movetime = 10;
    outcome_t game_state = outcome_t::UNDECIDED;
    while(1) {
        game_state = api.IsGameOver(board);
        if (game_state != outcome_t::UNDECIDED) break;

        string move;
        do {
            if (player_turn == true) cin >> move;
            else move = api.GetMove();
        } while(board.ValidateMove(move) == false);

        board.MakeMove(move);
        api.MakeMove(move);
        PrintTable(board.table);

        #if STOCKFISH_ONLY == 0
        player_turn = !player_turn;
        #endif
    }

    // Declare winner
    if (game_state == outcome_t::DRAW) {
        std::cout << "Game ended in a draw" << std::endl;
        if (board.IsThreefoldRepetition()) {
            std::cout << "Threefold repetition" << std::endl;
        }
        else if (board.OnlyKingsRemain()) {
            std::cout << "Only kings remain" << std::endl;
        }
        else {
            std::cout << "Default draw, probably 50 move limit" << std::endl;
        }
    } 
    
    else {
        if (board.LastMoveColor() == color_n::WHITE) {
            std::cout << "White won" << std::endl;
        } else {
            std::cout << "Black won" << std::endl;
        }
    }

    board.CleanPieces(); // Free remaining memory    
    std::cout << "Game over!" << std::endl;
}
