#include "api.hpp"

void Robochess::MakeMove(string move) {
    move_text.append(" ");
    move_text.append(move);
    move_text.append("\n");

    SendCommand(move_text);
    move_text.erase(move_text.size() - 1);
}

string Robochess::GetMove() {
    string return_move;

    // Tell Stockfish to calculate move
    std::string start_cmd = "go movetime " + std::to_string(movetime);
    SendCommand(start_cmd);

    // Get the final response from Stockfish
    while(1) {
        return_move = ReadResponse();
        int comparison = return_move.compare(0, 8, "bestmove");    
       
        if (comparison == 0) {
            break;
        }
    }

    // Extract just the final move
    return_move.erase(0, 9); // "bestmove "

    size_t space_index = return_move.find(" ");

    // No space means Stockfish stopped pondering and output shortened
    if (space_index != std::string::npos) {
        return_move.erase(space_index, return_move.size());
    } else {
        return_move.erase(return_move.size() - 1);
    }

    return return_move;
}

void Robochess::IgnoreHeader() {
    ReadResponse();
}

Robochess::Robochess(int* pipe_rc, int* pipe_sf) {
    pipe_to_rc = pipe_rc;
    pipe_to_sf = pipe_sf;
}

void Robochess::SendCommand(string command) {
    command.push_back('\n');
    write(pipe_to_sf[W], command.c_str(), command.size());
}

string Robochess::ReadResponse() {
    string output;
    char bf;

    while(1) {
        read(pipe_to_rc[R], &bf, 1);
        output.push_back(bf);

        if (bf == '\n') break;
    }

    return output;
}

outcome_t Robochess::IsGameOver(Chessboard& board) {
    // Check if 50 moves has happened without capture or pawn move
    if (board.moves_since_capture >= 50) {
        return outcome_t::DRAW;
    }

    // If only kings remain, call it a draw
    if (board.OnlyKingsRemain() == true) {
        return outcome_t::DRAW;
    }

    if (board.IsThreefoldRepetition() == true) {
        return outcome_t::DRAW;
    }

    SendCommand(move_text);
    SendCommand("go perft 1");

    string response;
    while(1) {
        response = ReadResponse();
        if (response.compare(0, 16, "Nodes searched: ") == 0) break;
    }

    // Check if any legal moves
    if (response.compare(0, 17, "Nodes searched: 0") != 0) {
        return outcome_t::UNDECIDED;
    }

    // No legal moves, check if king in check or not
    SendCommand("d");
    while(1) {
        response = ReadResponse();
        if (response.compare(0, 10, "Checkers: ") == 0) break;
    }

    if (response.compare(0, 11, "Checkers: \n") == 0) {
        return outcome_t::DRAW; // Draw
    }
    
    return outcome_t::WIN; // Checker won
}
